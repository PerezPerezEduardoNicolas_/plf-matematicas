Conjuntos, Aplicaciones y funciones 
```plantuml
@startmindmap
*[#red] Conjuntos, Aplicaciones y funciones
**[#Orange] Los conjuntos
***_ Su inclusion de conjuntos
****[#lightblue] Un conjunto esta contenido en otro si un todo elemento\n del primer conjunto pertenece al otro conjunto
***_ Sus operaciones son
****[#lightblue] Interseccion
*****[#FFBBCC] Elementos que pertenecen simultaneamente a ambos
****[#lightblue] Union
*****_ Se trata de 
****** Elementos que pertenecen por lo menos a alguno de ellos
****[#lightblue] Complementacion de conjuntos
*****_ Esto se refiere a
****** Que no pertenecen a algun conjunto dado
***_ Se representan por medio de
****[#lightblue] Diagramas de Venn
****_ Caracteristicas
*****[#FFBBCC] Es un recurso didactico mas caracteristico de la inclusion de conjuntos
*****[#FFBBCC] Ayudan a comprender intuitivamebnte la posicion de las operaciones
******_ Tales como
******* Interseccion
******* Union
*****_ Fue creado por
****** Jonh Venn
*****_ Una de sus desventajas es
****** No sirven como metodo de demostracion
***[#lightgreen] Los tipos de conjuntos
**** Universal
*****_ Caracterisados por 
****** Un conjunto de referencia en que ocurre toda la teoria
****[#lightblue] Vacio
*****_ Se caracteriza por
****** Tener necesidad logica para cerrar el conjunto que no tiene elemento
***_ El cardinal de un conjunto
****[#lightblue] Es un numero natural
****[#lightblue] El numero de elementos que conforman el conjunto
****_ caracterizados por
*****[#FFBBCC] Formulas
****[#lightblue] Cardinal de la union de conjuntos
*****_ es
****** Es igual al cardinal de uno de los conjuntos mas el cardinal del\nsegundo conjunto menos el cardinal\n de la interseccion
***[#lightgreen] Acotacion de cardinales
***_ esta
****[#lightblue] Basada en la formula cardinal de union de conjuntos
*****_ con el fin de
****** Encontrar una serie de relaciones
*******_ En base a
******** Quien es el mayor, menor o igual entre cardinales de conjuntos
**[#Orange] Sus aplicaciones y funciones son
***[#lightgreen] Diversas diciplinas cientificas
***[#lightgreen] Estudia las transformaciones 
****[#lightblue] Aplicacion 
*****_ Se describe como una transformacion o regla
****** Convirtiendo a cada uno de los elementos de un determinado conjunto inicial
******* En un unico elemento final
****** Tipos de Aplicaciones
******* Aplicacion inyectiva
******* Aplicacion sobreyectiva
******* Aplicacion biyectiva 
****[#lightblue] Funcion
*****_ ¿Cuando se utiliza la palabra funcion?
****** Cuando se tiene una transformacion o aplicacion entre conjuntos de numeros
*******_ Caracteristicas
******** Particularidad
*********_ Significa
********** Conjuntos que se transforman son conjuntos de numeros
******** Grafica de la funcion
********* Sistema de coordenadas
**********_ se obtiene
*********** Una figura
********* Serie de puntos
**********_ se obtiene
*********** Se obtienen conjunto de los puntos cuya primera coordenada del plano es el elemento del primer conjunto  
@endmindmap
```

Funciones (2010)
```plantuml
@startmindmap
*[#red] Funciones 
**_ Es 
***[#lightgreen] una obligación especial
****[#lightblue] Los conjuntos que se estan relacionando son conjuntos de numeros
**[#Orange] ¿Cual es su representacion grafica?
***[#lightgreen] Representacion cartesiana
****[#lightblue] Representacion en un plano
****[#lightblue] Eje de las imagenes
****_ fue creado por
*****[#FFBBCC] Rene Descartes
**[#Orange] Funciones crecientes y decrecientes
***_ Funciones crecientes
****[#lightblue] Aumenta la variable independiente del primer conjunto
****[#lightblue] Aumentan sus imagenes
***_ Funciones decrecientes
****[#lightblue] Disminuyen las variables
****[#lightblue] Aumentas las imagenes al mismo tiempo que sus variables
**[#Orange] Comportamiento
***[#lightgreen] Maximo
****_ Es cuando
*****[#FFBBCC] Cuando ya ha pasado por el maximo de la grafica
***[#lightgreen] Minimo
****_ Es cuando
*****[#FFBBCC] Cuando decrecio el valor minimo de la grafica
**[#Orange] El limite
***_ Conceptos
****[#lightblue] Cuando los valores de la funcion es tan cerca de sus imagenes
****[#lightblue] se considera un valor que esta cerca del punto de sus imagenes de un determinado valor
*****_ Debido a que
****** Es continua
****** No hay saltos en los valores
***_ Pretende 
****[#lightblue] Aproximar los valores
**[#Orange] Derivadas 
***_ Conceptos
****[#lightblue] Funciones sencillas
*****_ son
****** Las funciones lineales
****[#lightblue] La idea principal es aproximar una funcion muy complicada hacia una mas sencilla
****[#lightblue] ¿Que es una derivada?
*****[#FFBBCC] Es la aproximacion de la funcion mediante una funcion mas sencilla
****[#lightblue] ¿Que es lo que resuelve?
*****[#FFBBCC] El problema de la aproximacion de una funcion compleja		
@endmindmap
```

La matemática del computador (2002)
```plantuml
@startmindmap
*[#red] La matematica del computador
**_ ¿Que utiliza? 
***[#lightgreen] Aritmetica finita
****_ Conformado por
*****[#FFBBCC] Limitaciones en los medios fisicos
******_ por lo tanto
******* No es posible representar los numeros con infinitos decimales
*****[#FFBBCC] Numeros finitos de posiciones
******_ Se puede
******* Convertir 0 y 1 en una serie de posiciones de la memoria
**[#Orange] ¿En que se basa?
***[#lightgreen] Se basa en el sistema binario
****_ ¿Que es?
*****[#FFBBCC] Es un sistema simple que cuenta con 
****** Dos posiciones
****** Enlazado con la logica booleana
*******_ y
******** La pertenencia y no pertenencia en conjuntos
****_ ¿Que utiliza?
*****[#FFBBCC] Aritmetica binaria
****_ ¿Con que se construye?
*****[#FFBBCC] Con un sistema de numeracion
******_ caracteristicas
******* Se pasa corriente para 1
******* Se impide la corriente para 0
****[#lightblue] Sistema hexadecimal (6) y octal (8)
*****_ son
****** Representaciones compactas de numeros empleados en informatica
**[#Orange] El truncamiento y redondeo
***_ ¿Cuando sucede?
****[#lightblue] Cuando el numero que queremos representar tiene muchas cifras ya sea enteras o decimales
*****_ ¿Que se tiene que hacer?
****** Acotar la representacion de ese numero
*******_ se debe
******** Despreciarlas y retocar la ultima cifra que no despreciamos de un modo adecuado
*********_ ¿Para que hacer esto?
********** Para intentar cometer el menor error posible
**_ tambien utiliza
***[#lightgreen] La codificacion
****_ ¿que se puede hacer?
*****[#FFBBCC] Implementar representaciones de
****** Simbolos
****** Letras
****** Numeros
*****[#FFBBCC] Numeros enteros
******_ ¿cual es su representacion?
******* Magnitud del signo
*****[#FFBBCC] Se puede representar
****** Numeros muy grandes
****** Numeros muy pequeños
*******_ para que se usan
******** Notacion cientifica
*********_ realiza 
********** Conversiones acortando las cifras en binario
********* En forma del producto de un numero con la potencia 10
@endmindmap
```


